package com.app.calculate;

public interface Calculate {
    int calculate(int a, int b, int c);
}
