package com.app.command;

public interface Command {
    public void execute();
}
