import com.app.calculate.Calculate;

import java.util.Scanner;

public class CalculateTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a, b, c;

        System.out.print("Enter 3 numbers: ");
        String[] mass = scanner.nextLine().split(" ");

        a = Integer.parseInt(mass[0]);
        b = Integer.parseInt(mass[1]);
        c = Integer.parseInt(mass[2]);

        System.out.print("Max of numbers: ");
        Calculate calculateMax = (x, y, z) -> (x >= y) && (x >= z) ? x : (y >= z) ? y : z;
        int max = calculateMax.calculate(a, b, c);
        System.out.println(max);

        System.out.print("Average value: ");
        Calculate calculateAverage = (x, y, z) -> (int)((x + y + z) / 3);
        int average = calculateAverage.calculate(a, b, c);
        System.out.println(average);
    }

}
